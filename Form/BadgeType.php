<?php

namespace ATM\BadgeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BadgeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,array(
                'required' => true
            ))
            ->add('description',TextareaType::class,array(
                'required' => true
            ))
            ->add('image',FileType::class,array(
                'required' => true,
            ))
            ->add('userRoles',ChoiceType::class,array(
                'choices' => array(
                    'ROLE_USER' => 'USER',
                    'ROLE_GIRL' => 'GIRL'
                ),
                'required' => true,
                'expanded' => true,
                'multiple' => true
            ))
            ->add('relatedEntity',TextType::class,array(
                'required' => true
            ))
            ->add('engine',TextType::class,array(
                'required' => true
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'atmbadge_bundle_badge_type';
    }
}
