<?php

namespace ATM\BadgeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
class RuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search',TextType::class,array(
                'required' => true
            ))
            ->add('stringValue',TextType::class,array(
                'required' => false
            ))
            ->add('numericValue',TextType::class,array(
                'required' => false
            ))
            ->add('isNullValue',CheckboxType::class,array(
                'required' => false
            ))
            ->add('isNotNullValue',CheckboxType::class,array(
                'required' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'badgeNamespace' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'atmbadge_bundle_rule_type';
    }
}
