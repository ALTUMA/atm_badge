<?php

namespace ATM\BadgeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\BadgeBundle\Form\BadgeType;
use ATM\BadgeBundle\Form\RuleType;


class BadgeController extends Controller
{
    public function createBadgeAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $config = $this->getParameter('atm_badge_config');

        $badge = new $config['class']['model']['badge'];
        $form = $this->createForm(BadgeType::class,$badge);

        $arrErrors = null;
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $badgeImagesPath = $this->get('kernel')->getRootDir().'/../web/badges';
                if(!is_dir($badgeImagesPath)){
                    mkdir($badgeImagesPath,0777);
                }

                $file = $form['image']->getData();
                $file->move($badgeImagesPath,$file->getClientOriginalName());

                $badge->setImage('badges/'.$file->getClientOriginalName());
                $em->persist($badge);
                $em->flush();

                return new RedirectResponse($this->get('router')->generate('atm_badge_list_badges'));
            }else{
                $errors = $form->getErrors();
                $arrErrors = array();
                foreach($errors as $error ){
                    $arrErrors[] = $error->getMessage();
                }
            }
        }

        return $this->render('ATMBadgeBundle:Badge:create.html.twig',array(
            'form' => $form->createView(),
            'errors' => $arrErrors
        ));
    }

    public function listBadgesAction(){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_badge_config');

        $badges = $em->getRepository($config['class']['model']['badge'])->findAll(array(),array('id','ASC'));

        return $this->render('ATMBadgeBundle:Badge:list.html.twig',array(
            'badges' => $badges
        ));
    }

    public function createRulesAction($badgeId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_badge_config');
        $badge= $em->getRepository($config['class']['model']['badge'])->findOneById($badgeId);
        $request = $this->get('request_stack')->getCurrentRequest();


        $rule = new $config['class']['model']['rule'];
        $form = $this->createForm(RuleType::class,$rule,array(
            'badgeNamespace' => $config['class']['model']['badge']
        ));

        $arrErrors = null;
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);

            $rule->setBadge($badge);

            if($form->isValid()){
                $em->persist($rule);
                $em->flush();
                return new RedirectResponse($this->get('router')->generate('atm_badge_list_badges'));
            }else{
                $errors = $form->getErrors();
                $arrErrors = array();
                foreach($errors as $error ){
                    $arrErrors[] = $error->getMessage();
                }
            }
        }

        return $this->render('ATMBadgeBundle:Rule:create.html.twig',array(
            'form' => $form->createView(),
            'errors' => $arrErrors,
            'badge' => $badge
        ));
    }
}
