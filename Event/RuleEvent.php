<?php

namespace ATM\BadgeBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class RuleEvent extends Event{

    const NAME = 'atm_badge_rule.event';
    private $user;
    private $relatedEntity;
    private $relatedEntityId;

    public function __construct($user, $relatedEntity, $relatedEntityId = null){
        $this->user = $user;
        $this->relatedEntity = $relatedEntity;
        $this->relatedEntityId = $relatedEntityId;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getRelatedEntity()
    {
        return $this->relatedEntity;
    }

    public function setRelatedEntity($relatedEntity)
    {
        $this->relatedEntity = $relatedEntity;
    }

    public function getRelatedEntityId()
    {
        return $this->relatedEntityId;
    }

    public function setRelatedEntityId($relatedEntityId)
    {
        $this->relatedEntityId = $relatedEntityId;
    }
}