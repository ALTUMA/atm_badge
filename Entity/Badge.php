<?php

namespace ATM\BadgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class Badge{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    protected $description;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    protected $image;

    /**
     * @ORM\Column(name="related_entity", type="string", length=255, nullable=false)
     */
    protected $relatedEntity;

    /**
     * @ORM\Column(name="roles", type="array", nullable=false)
     */
    protected $userRoles;

    /**
     * @ORM\Column(name="engine", type="string", nullable=false)
     */
    protected $engine;


    public function __construct()
    {
        $this->userRoles = array();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getRelatedEntity()
    {
        return $this->relatedEntity;
    }

    public function setRelatedEntity($relatedEntity)
    {
        $this->relatedEntity = $relatedEntity;
    }

    public function getUserRoles()
    {
        return $this->userRoles;
    }

    public function setUserRoles($userRoles)
    {
        $this->userRoles = $userRoles;
    }

    public function getEngine()
    {
        return $this->engine;
    }

    public function setEngine($engine)
    {
        $this->engine = $engine;
    }
}