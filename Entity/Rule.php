<?php

namespace ATM\BadgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class Rule{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="search", type="string", length=100, nullable=false)
     */
    protected $search;

    /**
     * @ORM\Column(name="string_value", type="string", length=100, nullable=true)
     */
    protected $stringValue;

    /**
     * @ORM\Column(name="numeric_value", type="integer", nullable=true)
     */
    protected $numericValue;

    /**
     * @ORM\Column(name="is_null_value", type="boolean", nullable=true)
     */
    protected $isNullValue;

    /**
     * @ORM\Column(name="is_not_null_value", type="boolean", nullable=true)
     */
    protected $isNotNullValue;


    public function getId()
    {
        return $this->id;
    }

    public function getSearch()
    {
        return $this->search;
    }

    public function setSearch($search)
    {
        $this->search = $search;
    }

    public function getStringValue()
    {
        return $this->stringValue;
    }

    public function setStringValue($stringValue)
    {
        $this->stringValue = $stringValue;
    }

    public function getNumericValue()
    {
        return $this->numericValue;
    }

    public function setNumericValue($numericValue)
    {
        $this->numericValue = $numericValue;
    }

    public function getIsNullValue()
    {
        return $this->isNullValue;
    }

    public function setIsNullValue($isNullValue)
    {
        $this->isNullValue = $isNullValue;
    }

    public function getIsNotNullValue()
    {
        return $this->isNotNullValue;
    }

    public function setIsNotNullValue($isNotNullValue)
    {
        $this->isNotNullValue = $isNotNullValue;
    }
}