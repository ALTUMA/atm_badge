<?php

namespace ATM\BadgeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

abstract class Reward{

    /**
     * @ORM\Column(name="won_at", type="datetime", nullable=false)
     */
    protected $wonAt;


    public function __construct()
    {
        $this->wonAt = new DateTime();
    }

    public function getWonAt()
    {
        return $this->wonAt;
    }

    public function setWonAt($wonAt)
    {
        $this->wonAt = $wonAt;
    }
}